const { db } = require("../../config/db");
const Exception = require("../errorHandler/exception");
const errorCode = require("../errorHandler/errorCodes");
const CustomField = db.customFields;
const params = require("params");

const findFieldById = async (id) => {
  try {
    return await CustomField.findOne({ where: { id } });
  } catch (error) {
    throw error;
  }
};
const addFieldWithTransaction = async (data, t) => {
  try {
    let customField = new CustomField({
      title_ar: data.title_ar,
      title_en: data.title_en,
      list_en: data.list_en,
      list_ar: data.list_ar,
      productId: data.productId,
    });
    await customField.save({ transaction: t });
    return;
  } catch (error) {
    throw error;
  }
};
const updateFieldWithTransaction = async (data, t) => {
  try {
    let field = await findFieldById(data.id);
    if (!field || field.productId != data.productId)
      throw new Exception(errorCode.INVALID_INPUT);
    await field.update(params(data).only(permittedParams()), {
      transaction: t,
    });
  } catch (error) {
    throw error;
  }
};

const deleteFieldWithTransaction = async (data, t) => {
  try {
    let field = await findFieldById(data.id);
    if (!field || field.productId != data.productId)
      throw new Exception(errorCode.INVALID_INPUT);
    await field.destroy({}, { transaction: t });
    return;
  } catch (error) {
    throw error;
  }
};

const upSertDel = async (data, t) => {
  try {
    if (data.isDeleted) {
      await deleteFieldWithTransaction(data, t);
    } else if (data.id) {
      await updateFieldWithTransaction(data, t);
    } else {
      await addFieldWithTransaction(data, t);
    }
  } catch (error) {
    throw error;
  }
};

const permittedParams = () => {
  return ["title_ar", "title_en", "list_ar", "list_en"];
};

module.exports = {
  addFieldWithTransaction,
  updateFieldWithTransaction,
  deleteFieldWithTransaction,
  upSertDel,
  findFieldById,
};
