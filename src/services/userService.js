const { db } = require("../../config/db");
const User = db.users;
const Exception = require("../errorHandler/exception");
const errorCodes = require("../errorHandler/errorCodes");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const findUserByEmail = async (userEmail) => {
  try {
    let user = await User.findOne({ where: { email: userEmail } });
    return user;
  } catch (error) {
    throw error;
  }
};
const findUserById = async (userId) => {
  try {
    let user = await User.findOne({ where: { id: userId } });
    return user;
  } catch (error) {
    throw error;
  }
};

const checkLoginAbility = async (userEmail, password) => {
  try {
    let user = await findUserByEmail(userEmail);

    if (!user) throw new Exception(errorCodes.INCORRECT_LOGIN);

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) throw new Exception(errorCodes.INCORRECT_LOGIN);

    const token = jwt.sign(
      { id: user.id, email: user.email },
      process.env.jWTSECRET,
      {
        expiresIn: "4h",
      }
    );

    return { token, id: user.id, email: user.email, expiresIn: "4h" };
  } catch (error) {
    throw error;
  }
};

module.exports = {
  checkLoginAbility,
  findUserByEmail,
  findUserById,
};
