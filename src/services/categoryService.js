const Exception = require("../errorHandler/exception");
const errorCodes = require("../errorHandler/errorCodes");
const { db } = require("../../config/db");
const Category = db.categories;
const Product = db.products;
const productLang = require("../language/productLanguage");
const categoryLang = require("../language/categoryLanguage");
const { Op, Sequelize } = require("sequelize");

const findAllCategory = async (isArabic, limit, offset) => {
  const language = isArabic
    ? categoryLang.ArabicFields
    : categoryLang.EnglishFields;

  try {
    let categories;
    if (!limit || !offset) {
      categories = await Category.findAll({
        attributes: language,
      });
    } else {
      categories = await Category.findAndCountAll({
        attributes: language,
        limit,
        offset,
      });
    }
    return categories;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

const findCategoryById = async (categoryId) => {
  try {
    let category = await Category.findOne({ where: { id: categoryId } });
    return category;
  } catch (error) {
    throw error;
  }
};

const deleteCategoryById = async (categoryId) => {
  try {
    let category = await findCategoryById(categoryId);
    if (!category) throw new Exception(errorCodes.NOT_FOUND);
    await category.destroy();
    return;
  } catch (error) {
    throw error.httpStatus ? error : new Error(error.message);
  }
};

const findCategoryProducts = async (isArabic, id) => {
  try {
    let language = isArabic ? "ArabicFields" : "EnglishFields";
    let categoryProducts;
    let now = new Date();
    categoryProducts = await Category.findOne({
      where: {
        id,
      },
      attributes: categoryLang[language],
      include: [
        {
          model: Product,
          attributes: productLang[language],
          as: "products",
          required: false,
          where: {
            "$products.startDate$": {
              [Op.lte]: now,
              [Op.gte]: Sequelize.literal(
                `NOW()-"products"."duration"*interval '1d'`
              ),
            },
          },
        },
      ],
    });
    return categoryProducts;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  findAllCategory,
  findCategoryById,
  deleteCategoryById,
  findCategoryProducts,
};
