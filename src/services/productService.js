const Exception = require("../errorHandler/exception");
const errorCodes = require("../errorHandler/errorCodes");
const productLanguage = require("../language/productLanguage");
const categoryServices = require("./categoryService");
const customFieldServices = require("./customFieldService");
const { sequelize, db } = require("../../config/db");
const Category = db.categories;
const Product = db.products;
const CustomField = db.customFields;
const customLang = require("../language/coustomFieldLanguage");
const productLang = require("../language/productLanguage");
const categoryLang = require("../language/categoryLanguage");
const { Op, Sequelize } = require("sequelize");
const params = require("params");

const findProductById = async (id, isDetails, isArabic) => {
  let language = isArabic ? "ArabicFields" : "EnglishFields";
  try {
    let product;
    if (isDetails) {
      product = await Product.findOne({
        where: { id },
        attributes: productLang[language],
        include: [
          {
            model: CustomField,
            as: "customFields",
            attributes: customLang[language],
          },
          { model: Category, attributes: categoryLang[language] },
        ],
      });
      if (!product) throw new Exception(errorCodes.INVALID_INPUT);
      return product;
    } else {
      product = await Product.findOne({ where: { id } });
      return product;
    }
  } catch (error) {
    throw error;
  }
};

const findAllProduct = async (isArabic, limit, offset) => {
  const language = isArabic
    ? productLanguage.ArabicFields
    : productLanguage.EnglishFields;
  let now = new Date();
  try {
    let products;
    if (!limit || !offset) {
      products = await Product.findAll({
        where: {
          startDate: {
            [Op.lte]: now,
            [Op.gte]: Sequelize.literal(`NOW()-"duration"*interval '1d'`),
          },
        },
        attributes: language,
      });
    } else {
      products = await Product.findAndCountAll({
        where: {
          startDate: {
            [Op.lte]: now,
            [Op.gte]: Sequelize.literal(`NOW()-"duration"*interval '1d'`),
          },
        },
        attributes: language,
        limit,
        offset,
      });
    }
    return products;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

const addProduct = async (data) => {
  const t = await sequelize.transaction();
  try {
    const {
      name_en,
      name_ar,
      startDate,
      duration,
      price,
      categoryId,
      customFields,
    } = data;
    let product = new Product({
      name_en,
      name_ar,
      startDate,
      duration,
      price,
      categoryId,
    });
    let category = await categoryServices.findCategoryById(categoryId);
    if (!category) throw new Exception(errorCodes.INVALID_INPUT);

    await product.save({ transaction: t });

    await Promise.all(
      customFields.map(async (el) => {
        return await customFieldServices.addFieldWithTransaction(
          {
            ...el,
            productId: product.id,
          },
          t
        );
      })
    );

    await t.commit();
    return data;
  } catch (error) {
    await t.rollback();
    throw error;
  }
};
const updateProduct = async (data) => {
  const t = await sequelize.transaction();
  try {
    let product = await findProductById(data.id);
    if (!product) throw new Exception(errorCodes.INVALID_INPUT);
    const { customFields } = data;
    await product.update(params(data).only(permittedParams()), {
      transaction: t,
    });
    if (customFields) {
      await Promise.all(
        customFields.map(async (el) => {
          return await customFieldServices.upSertDel(
            { ...el, productId: product.id },
            t
          );
        })
      );
    }
    await t.commit();
    let response = await Product.findOne({
      where: { id: data.id },
      include: [
        { model: CustomField, as: "customFields" },
        { model: Category },
      ],
    });
    return response;
  } catch (error) {
    await t.rollback();
    throw error;
  }
};

const deleteProductById = async (id) => {
  try {
    let product = await findProductById(id);
    if (!product) throw new Exception(errorCodes.INVALID_INPUT);
    await product.destroy();
    return;
  } catch (error) {
    throw error;
  }
};

const permittedParams = () => {
  return ["name_ar", "name_en", "duration", "startDate", "price"];
};
module.exports = {
  findAllProduct,
  findProductById,
  addProduct,
  updateProduct,
  deleteProductById,
};
