const fieldsLanguageHandler = require("../language/index");

module.exports = (sequelize, DataTypes) => {
  const CoustomField = sequelize.define(
    "customFields",
    {
      title_ar: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      title_en: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      list_en: {
        type: DataTypes.JSON,
      },
      list_ar: {
        type: DataTypes.JSON,
      },
    },
    {}
  );

  fieldsLanguageHandler(CoustomField.rawAttributes, "coustomFieldLanguage");
  return CoustomField;
};
