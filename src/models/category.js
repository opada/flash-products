const fieldsLanguageHandler = require("../language/index");
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define(
    "categories",
    {
      name_ar: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      name_en: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
    },
    {
      timestamps: false,
    }
  );
  Category.associate = (models) => {
    Category.hasMany(models.products, {
      foreignKey: "categoryId",
      sourceKey: "id",
      onDelete: "cascade",
      hooks: true,
    });
    models.products.belongsTo(Category, {
      foreignKey: "categoryId",
      targetKey: "id",
    });
    fieldsLanguageHandler(Category.rawAttributes, "categoryLanguage");
  };

  return Category;
};
