const fieldsLanguageHandler = require("../language/index");

module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define(
    "products",
    {
      name_ar: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      name_en: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      startDate: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      //per day
      duration: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      price: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
    },
    {}
  );

  Product.associate = (models) => {
    Product.hasMany(models.customFields, {
      as: "customFields",
      foreignKey: "productId",
      sourceKey: "id",
      onDelete: "cascade",
      hooks: true,
    });
    models.customFields.belongsTo(Product, {
      foreignKey: "productId",
      targetKey: "id",
      as: "product",
    });
    fieldsLanguageHandler(Product.rawAttributes, "productLanguage");
  };
  return Product;
};
