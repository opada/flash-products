const httpStatus = require("./httpStatus");

class Exception extends Error {
  constructor(error) {
    super(error.message);
    this.errorCode = error.code;
    this.httpStatus = error.httpStatus;
    this.arabicMessage = error.arabicMessage;
  }

  static exceptionHandeler(error, req, res, next) {
    const { ln } = req.query;
    console.log(error);
    console.log(error.stack);
    res.status(error.httpStatus || httpStatus.INTERNAL_SERVER_ERROR).json({
      error_code: error.errorCode,
      message:
        ln == "ar" && error.arabicMessage ? error.arabicMessage : error.message,
    });
  }
}

module.exports = Exception;
