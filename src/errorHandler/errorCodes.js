const httpStatus = require("./httpStatus");

module.exports = {
  UNAUTHORIZED: {
    code: 1003,
    message: "Authentication failed",
    arabicMessage: "فشلت المصادقة",
    httpStatus: httpStatus.UNAUTHRORIZED,
  },
  NOT_FOUND: {
    code: 1004,
    message: "Not found",
    arabicMessage: "غير موجود",
    httpStatus: httpStatus.NOT_FOUND,
  },
  INVALID_INPUT: {
    code: 1005,
    message: "Invalid input",
    arabicMessage: "خطأ في الادخال",
    httpStatus: httpStatus.UNPROCESSABLE_ENTITY,
  },
  INCORRECT_LOGIN: {
    code: 1006,
    message: "Invalid email or password.",
    arabicMessage: "البريد الالكتروني أو كلمة السر غير صحيحة",
    httpStatus: httpStatus.BAD_REQUEST,
  },
};
