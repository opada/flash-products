const ProductServices = require("../services/productService");
const httpStatus = require("../errorHandler/httpStatus");

const getProducts = async (req, res, next) => {
  const { ln, limit, offset } = req.query;
  try {
    let products = await ProductServices.findAllProduct(
      ln === "ar",
      limit,
      offset
    );
    return res.status(httpStatus.OK).json(products);
  } catch (error) {
    next(error);
  }
};
const addProuct = async (req, res, next) => {
  try {
    let product = await ProductServices.addProduct(req.body);
    return res.status(httpStatus.CREATED).json(product);
  } catch (error) {
    next(error);
  }
};

const updateProduct = async (req, res, next) => {
  try {
    let id = req.params.productId;
    let product = await ProductServices.updateProduct({ ...req.body, id });
    return res.status(httpStatus.UPDATED).json(product);
  } catch (error) {
    next(error);
  }
};

const deleteProduct = async (req, res, next) => {
  try {
    await ProductServices.deleteProductById(req.params.productId);
    return res.status(httpStatus.OK).json({
      message: req.query.ln == "ar" ? "تم الحذف بنجاح" : "Deleted successfully",
    });
  } catch (error) {
    next(error);
  }
};

const getProductDetails = async (req, res, next) => {
  try {
    let productId = req.params.productId;
    let productDetails = await ProductServices.findProductById(
      productId,
      true,
      req.query.ln == "ar"
    );
    return res.status(httpStatus.OK).json(productDetails);
  } catch (error) {
    next(error);
  }
};
module.exports = {
  getProducts,
  addProuct,
  updateProduct,
  deleteProduct,
  getProductDetails,
};
