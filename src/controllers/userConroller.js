const userServices = require("../services/userService");
const httpStatus = require("../errorHandler/httpStatus");
const login = async (req, res, next) => {
  const { email, password } = req.body;
  try {
    let response = await userServices.checkLoginAbility(email, password);
    return res.status(httpStatus.OK).json(response );
  } catch (error) {
    next(error);
  }
};

module.exports = {
  login,
};
