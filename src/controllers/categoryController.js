const CategoryServices = require("../services/categoryService");
const httpStatus = require("../errorHandler/httpStatus");

const getCategories = async (req, res, next) => {
  const { ln, limit, offset } = req.query;

  try {
    let categories = await CategoryServices.findAllCategory(
      ln === "ar",
      limit,
      offset
    );
    return res.status(httpStatus.OK).json(categories);
  } catch (error) {
    next(error);
  }
};

const getCategoryProducts = async (req, res, next) => {
  try {
    const { ln } = req.query;
    const { categoryId } = req.params;
    let products = await CategoryServices.findCategoryProducts(
      ln == "ar",
      categoryId
    );
    return res.status(httpStatus.OK).json(products);
  } catch (error) {
    next(error);
  }
};

const deleteCategory = async (req, res, next) => {
  const { id } = req.params;
  try {
    await CategoryServices.deleteCategoryById(id);

    return res.status(httpStatus.OK).json({
      message: req.query.ln == "ar" ? "تم الحذف بنجاح" : "Deleted successfully",
    });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getCategories,
  deleteCategory,
  getCategoryProducts,
};
