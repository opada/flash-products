const routers = (app) => {
  //user..
  app.use("/auth", require("./userRouter"));
  //category
  app.use("/categories", require("./categoryRouter"));
  //product
  app.use("/products", require("./productRouter"));
};

module.exports = routers;
