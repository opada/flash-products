const express = require("express");

//controller
const categoryController = require("../controllers/categoryController");

const authMiddleware = require("../middlewares/auth");

const router = express.Router();

//routers
router.get("/", categoryController.getCategories);

router.get("/:categoryId/products", categoryController.getCategoryProducts);

router.delete("/:id", authMiddleware, categoryController.deleteCategory);

module.exports = router;
