const express = require("express");

//controller
const userController = require("../controllers/userConroller");

//validation
const userValidator = require("../middlewares/validation/userValidator");


const router = express.Router();

//user routers
router.post("/login", userValidator.login, userController.login);

module.exports = router;
