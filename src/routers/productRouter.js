const express = require("express");

//controller
const productController = require("../controllers/productController");

//validator
const productValidator = require("../middlewares/validation/productValidator");

const authMiddleware = require("../middlewares/auth");

const router = express.Router();

//routers
router.get("/", productController.getProducts);
router.post(
  "/",
  authMiddleware,
  productValidator.addProduct,
  productController.addProuct
);
router.put(
  "/:productId",
  authMiddleware,
  productValidator.updateProduct,
  productController.updateProduct
);
router.delete("/:productId", authMiddleware, productController.deleteProduct);
//details
router.get("/:productId", productController.getProductDetails);
module.exports = router;
