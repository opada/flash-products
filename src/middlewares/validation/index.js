const { validationResult } = require("express-validator");
//error handler
const errorCodes = require("../../errorHandler/errorCodes");
const Exception = require("../../errorHandler/exception");

const errorHandeler = (req, res, next) => {
  let { ln } = req.query;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let exception = new Exception(errorCodes.INVALID_INPUT);
    exception.errors = errors.array();
    return res.status(exception.httpStatus).json({
      error: {
        code: exception.errorCode,
        message:
          ln == "ar" && exception.arabicMessage
            ? exception.arabicMessage
            : exception.message,
        errors: exception.errors,
      },
    });
  }
  return next();
};

module.exports = errorHandeler;
