const { body } = require("express-validator");
const errorHandeler = require("./index");
const loginChain = [
  body("email", {
    arabicMessage: "الرجاء ادخال بريد الكتروني صحيح",
    englishMessage: "Please enter a valid email",
  })
    .isEmail()
    .normalizeEmail(),
  body("password", {
    arabicMessage: "الرجاء ادخال كلمة السر",
    englishMessage: "Please enter password",
  }).notEmpty(),
];

module.exports = {
  login: [loginChain, errorHandeler],
};
