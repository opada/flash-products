const { body } = require("express-validator");
const errorHandeler = require("./index");
const addProductChain = [
  body("name_en", {
    arabicMessage: "الرجاء ادخال الاسم",
    englishMessage: "Please enter a name",
  }).notEmpty(),
  body("name_ar", {
    arabicMessage: "الرجاء ادخال الاسم",
    englishMessage: "Please enter a name",
  }).notEmpty(),
  body("startDate")
    .notEmpty()
    .withMessage({
      arabicMessage: "الرجاء ادخال تاريخ البداية",
      englishMessage: "Please enter start date",
    })
    .isISO8601()
    .withMessage({
      arabicMessage: "قيمة الحقل يجب ان تكون تاريخ",
      englishMessage: "startDate should be a date",
    }),
  body("duration", {
    arabicMessage: "قيمة الحقل يجب ان تكون رقم صحيح",
    englishMessage: "duration should be a int",
  })
    .notEmpty()
    .withMessage({
      arabicMessage: "الحقل مطلوب",
      englishMessage: "duration require",
    })
    .isNumeric(),
  body("price", {
    arabicMessage: "قيمة الحقل يجب ان تكون رقم ",
    englishMessage: "duration should be a float",
  })
    .notEmpty()
    .withMessage({
      arabicMessage: "الحقل مطلوب",
      englishMessage: "price require",
    })
    .isNumeric(),
  body("categoryId", {
    arabicMessage: "الحقل مطلوب",
    englishMessage: "categoryId require",
  }).notEmpty(),
  body("customFields", {
    arabicMessage: "الحقل يجب ان يكون مصفوفة",
    englishMessage: "customFields should be array",
  })
    .optional()
    .isArray(),
  body("customFields.*.title_en", {
    arabicMessage: "الحقل مطلوب",
    englishMessage: "title_en require",
  }).notEmpty(),
  body("customFields.*.title_ar", {
    arabicMessage: "الحقل مطلوب",
    englishMessage: "title_ar require",
  }).notEmpty(),
];

const updateProductChain = [
  body("name_en", {
    arabicMessage: "الرجاء ادخال الاسم",
    englishMessage: "Please enter a name",
  })
    .optional()
    .notEmpty(),
  body("name_ar", {
    arabicMessage: "الرجاء ادخال الاسم",
    englishMessage: "Please enter a name",
  })
    .optional()
    .notEmpty(),
  body("startDate")
    .optional()
    .notEmpty()
    .withMessage({
      arabicMessage: "الرجاء ادخال تاريخ البداية",
      englishMessage: "Please enter start date",
    })
    .isISO8601()
    .withMessage({
      arabicMessage: "قيمة الحقل يجب ان تكون تاريخ",
      englishMessage: "startDate should be a date",
    }),
  body("duration", {
    arabicMessage: "قيمة الحقل يجب ان تكون رقم صحيح",
    englishMessage: "duration should be a int",
  })
    .optional()
    .notEmpty()
    .withMessage({
      arabicMessage: "الحقل مطلوب",
      englishMessage: "duration require",
    })
    .isNumeric(),
  body("price", {
    arabicMessage: "قيمة الحقل يجب ان تكون رقم ",
    englishMessage: "duration should be a float",
  })
    .optional()
    .notEmpty()
    .withMessage({
      arabicMessage: "الحقل مطلوب",
      englishMessage: "price require",
    })
    .isNumeric(),
  body("categoryId", {
    arabicMessage: "الحقل مطلوب",
    englishMessage: "categoryId require",
  })
    .optional()
    .notEmpty(),
  body("customFields", {
    arabicMessage: "الحقل يجب ان يكون مصفوفة",
    englishMessage: "customFields should be array",
  })
    .optional()
    .isArray(),
  body("customFields.*.title_en", {
    arabicMessage: "الحقل مطلوب",
    englishMessage: "title_en require",
  })
    .optional()
    .notEmpty(),
  body("customFields.*.title_ar", {
    arabicMessage: "الحقل مطلوب",
    englishMessage: "title_ar require",
  })
    .optional()
    .notEmpty(),
];

module.exports = {
  addProduct: [addProductChain, errorHandeler],
  updateProduct: [updateProductChain, errorHandeler],
};
