const jwt = require("jsonwebtoken");

const Exception = require("../errorHandler/exception");

const errorCodes = require("../errorHandler/errorCodes");

const exception = new Exception(errorCodes.UNAUTHORIZED);

require("dotenv").config();

const auth = async (req, res, next) => {
  try {
    if (!req.header("auth-token")) throw exception;

    const token = req.header("auth-token");

    if (!token) throw exception;

    const user = jwt.verify(token, process.env.jWTSECRET);

    if (!user) throw exception;

    req.user = user;

    next();
  } catch (error) {
    next(exception);
  }
};
module.exports = auth;
