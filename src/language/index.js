const fieldsLanguageHandler = (rawAttributes, fileName) => {
  let ArabicFields = [];
  let EnglishFields = [];
  for (let key in rawAttributes) {
    let lng = key.split("_")[1];
    if (lng == "ar") ArabicFields.push(key);
    else if (lng == "en") EnglishFields.push(key);
    else {
      ArabicFields.push(key);
      EnglishFields.push(key);
    }
  }
  let file = require(`./${fileName}`);
  file.ArabicFields = ArabicFields;
  file.EnglishFields = EnglishFields;
};

module.exports = fieldsLanguageHandler;
