CREATE TABLE IF NOT EXISTS "categories" (
	id serial PRIMARY KEY,
	name_en text UNIQUE NOT NULL,
	name_ar text UNIQUE NOT NULL
);
do $$
begin
if NOT EXISTS (SELECT * from "categories" LIMIT 1) then
   INSERT INTO "categories"(name_en,name_ar)
   VALUES ('real estate','عقارات'),('vehicle','مركبة');
end if;
end$$;
