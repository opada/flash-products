CREATE TABLE IF NOT EXISTS "users" (
	id serial PRIMARY KEY,
	email text UNIQUE NOT NULL,
	password text NOT NULL
);
do $$
begin
if NOT EXISTS (SELECT * from "users" LIMIT 1) then
   INSERT INTO "users"(email,password)
   VALUES ('opada.o@hotmail.com','$2a$10$j8qLmysZ6gbxeYvD/9htIOA1TpzJIPTzb3ks7v1QA64SJ/dYDo46e'),('admin@gmail.com','$2a$10$Xhox/eMvtK3vQGGJJZJSYOo9Ofv6t3qTgq.QI4Rv7USIwUxCRW0hu');
end if;
end$$;
