require("dotenv").config();
const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);

const { DB_NAME, DB_USERNAME, DB_PASSWORD, DB_PORT, DB_HOST } = process.env;
const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  DB_NAME || "postgres",
  DB_USERNAME || "postgres",
  DB_PASSWORD || "123456",
  {
    port: DB_PORT || 5432,
    dialect: "postgres",
    host: DB_HOST || "localhost",
  }
);

const models = path.join(__dirname, "/../src/models");
const db = {};
fs.readdirSync(models)
  .filter(function (file) {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach(function (file) {
    var model = require(path.join(models, file))(
      sequelize,
      Sequelize.DataTypes
    );
    db[model.name] = model;
  });
Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

module.exports = {
  sequelize,
  db,
};
