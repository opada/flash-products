const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const compression = require("compression");
const { sequelize } = require("./config/db");
const Exception = require("./src/errorHandler/exception");
const routers = require("./src/routers/index");

const port = process.env.PORT || 8080;

const app = express();

app.use(cors());

app.use(compression());

app.use(bodyParser.json());

//routers
routers(app);

// Error Handler
app.use(Exception.exceptionHandeler);

sequelize.sync().then(() => {
  app.listen(port, () => console.log(`app listening on port ${port}!`));
});

module.exports = app;
